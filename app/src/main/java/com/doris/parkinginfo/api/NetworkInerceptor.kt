package com.doris.parkinginfo.api

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response
import com.google.gson.Gson



class NetworkInerceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        return chain.proceed(request)
    }

}