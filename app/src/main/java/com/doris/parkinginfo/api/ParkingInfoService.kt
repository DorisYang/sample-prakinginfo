package com.doris.parkinginfo.api

import android.arch.lifecycle.LiveData
import com.doris.parkinginfo.vo.ParkingInfo

import retrofit2.http.GET
import java.util.ArrayList

interface ParkingInfoService {
    @GET("B1464EF0-9C7C-4A6F-ABF7-6BDF32847E68?\$format=json")
    fun getParkingInfo(): LiveData<ApiResponse<ArrayList<ParkingInfo>>>

}