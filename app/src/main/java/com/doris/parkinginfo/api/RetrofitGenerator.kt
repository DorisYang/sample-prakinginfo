package com.doris.parkinginfo.api

import com.doris.parkinginfo.util.LiveDataCallAdapterFactory
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

class RetrofitGenerator {
    companion object {
        val API_BASE_URL = "https://data.ntpc.gov.tw/od/data/api/"
        val httpClient = OkHttpClient.Builder()

        private var builder: Retrofit.Builder? = null
        private var retrofit: Retrofit? = null
        fun getRetrofit(): Retrofit? {
            if (retrofit == null) {
                synchronized(RetrofitGenerator::class.java) {
                    if (retrofit == null) {
                        httpClient.connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                           // .addInterceptor(NetworkInerceptor())

                        retrofit = getBuilder()!!.client(httpClient.build()).build()
                    }
                }
            }
            return retrofit
        }

        fun getBuilder(): Retrofit.Builder? {
            if (builder == null) {
                builder = Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(LiveDataCallAdapterFactory())
            }
            return builder

        }

        fun <S> createService(serviceClass: Class<S>): S {
            return getRetrofit()!!.create(serviceClass)
        }

    }


}