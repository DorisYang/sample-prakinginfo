package com.doris.parkinginfo.di

import com.doris.parkinginfo.api.ParkingInfoService
import com.doris.parkinginfo.api.RetrofitGenerator
import com.doris.parkinginfo.util.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApiServiceModule{
    @Singleton
    @Provides
    fun provideParkingInfoService(): ParkingInfoService {
        return RetrofitGenerator.createService(ParkingInfoService::class.java)
    }

}