package com.doris.parkinginfo.di

import com.doris.parkinginfo.BasicApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ApiServiceModule::class,
        BuildersModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: BasicApp): Builder
        fun build(): AppComponent
    }

    fun inject(mainApp: BasicApp)
}