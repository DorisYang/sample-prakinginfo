package com.doris.parkinginfo.ui.calendar

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View

class MonthBorder(context: Context, attrs: AttributeSet, defStyle: Int) : View(context, attrs, defStyle) {
    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0)

    private var paint: Paint
    var onfocus = false

    init {
        paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = Color.rgb(0, 59, 255)
            style = Paint.Style.STROKE
            strokeWidth = 10f
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (onfocus)
            canvas.drawRect(RectF(0f, 0f, right.toFloat(), bottom.toFloat()), paint)
    }

    fun setFocus() {
        onfocus = true
        invalidate()
    }

    fun cancelFocus() {
        onfocus = false
        invalidate()
    }

}