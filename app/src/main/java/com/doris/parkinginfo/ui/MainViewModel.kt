package com.doris.parkinginfo.ui

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.doris.parkinginfo.AppExecutors
import com.doris.parkinginfo.api.*
import com.doris.parkinginfo.util.AbsentLiveData
import com.doris.parkinginfo.util.Resource
import com.doris.parkinginfo.vo.ParkingInfo
import retrofit2.Call
import java.util.ArrayList
import javax.inject.Inject

class MainViewModel
@Inject constructor(
    val parkingInfoService: ParkingInfoService, private val appExecutors: AppExecutors) : ViewModel() {

    fun getParkingInfo(): LiveData<Resource<ArrayList<ParkingInfo>>> {
        return object : NetworkBoundResource<ArrayList<ParkingInfo>, ArrayList<ParkingInfo>>(appExecutors) {
            override fun saveCallResult(item: ArrayList<ParkingInfo>) {
            }

            override fun shouldFetch(data: ArrayList<ParkingInfo>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<ArrayList<ParkingInfo>> {
                return AbsentLiveData.create()
            }

            override fun createCall(): LiveData<ApiResponse<ArrayList<ParkingInfo>>> {
                return parkingInfoService.getParkingInfo()
            }

        }.asLiveData()
    }
}