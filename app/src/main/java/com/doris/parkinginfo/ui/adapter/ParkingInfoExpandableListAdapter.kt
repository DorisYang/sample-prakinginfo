package com.doris.parkinginfo.ui.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.doris.parkinginfo.R
import com.doris.parkinginfo.vo.ParkingInfo

class ParkingInfoExpandableListAdapter(parkingInfoList: List<ParkingInfo>) : BaseExpandableListAdapter() {
    private var dataList: Map<String, List<ParkingInfo>> = parkingInfoList.groupBy { it.area }
    var parentList = dataList.keys.toList()

    override fun getGroup(groupPosition: Int): String {
        return parentList.get(groupPosition)
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View? {
        var view = convertView
        val headTitle = getGroup(groupPosition)
        if (view == null) {
            var infalInflater = parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = infalInflater.inflate(R.layout.item_exlist_group, parent, false)
        }

        view?.let {
            val title = view.findViewById<TextView>(R.id.district)
            title.text = headTitle
        }

        return view
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return dataList.getValue(parentList.get(groupPosition)).size
    }

    override fun getChild(groupPosition: Int, childPosition: Int): ParkingInfo {
        return dataList.getValue(parentList.get(groupPosition)).get(childPosition)
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return parentList.size
    }

    override fun getChildView(
        groupPosition: Int, childPosition: Int, isLastChild: Boolean,
        convertView: View?, parent: ViewGroup
    ): View? {
        var view = convertView
        val parkingInfo = getChild(groupPosition, childPosition)
        if (view == null) {
            var infalInflater = parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = infalInflater.inflate(R.layout.item_exlist_item, parent, false)
        }

        view?.let {
            val name = view.findViewById<TextView>(R.id.name)
            name.text = parkingInfo.name
        }

        return view
    }


}