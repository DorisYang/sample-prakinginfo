package com.doris.parkinginfo.ui.common

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.webkit.WebView
import android.widget.ImageButton

import com.doris.parkinginfo.vo.ParkingInfo
import kotlinx.android.synthetic.main.dialog_parking_info_detail.*


object CustomDialog {
    private var loadingDialog: Dialog? = null
    private var dialog: Dialog? = null

    @Synchronized
    fun showLoadingDialog(activity: Activity) {
        if (loadingDialog == null)
            loadingDialog = createLoadingDialog(activity as Context)
        if (!activity.isFinishing) {
            loadingDialog?.show()
        }
    }

    fun closeLoadingDialog() {
        loadingDialog?.cancel()

    }

    fun createLoadingDialog(context: Context): Dialog {
        return Dialog(context).apply {
            setContentView(com.doris.parkinginfo.R.layout.dialog_loading)
            window.setBackgroundDrawableResource(android.R.color.transparent)
            setCancelable(false)
        }
    }

    @Synchronized
    fun showParkingInfoDetailDialog(activity: Activity, parkingInfo: ParkingInfo) {
        if (dialog == null)
            dialog = createParkingInfoDetailDialog(activity as Context)
        dialog?.setData(parkingInfo)
        if (!activity.isFinishing) {
            dialog?.show()
        }

    }

    fun createParkingInfoDetailDialog(context: Context): Dialog {
        return Dialog(context).apply {
            setContentView(com.doris.parkinginfo.R.layout.dialog_parking_info_detail)
            setCancelable(false)
            findViewById<ImageButton>(com.doris.parkinginfo.R.id.close).setOnClickListener {
                dialog?.cancel()
            }
        }
    }

    fun Dialog.setData(parkingInfo: ParkingInfo) = apply {
        name.text = context.getString(com.doris.parkinginfo.R.string.name, parkingInfo.name)
        address.text = context.getString(com.doris.parkinginfo.R.string.address, parkingInfo.address)
        service_time.text = context.getString(com.doris.parkinginfo.R.string.service_time, parkingInfo.serviceTime)
        payex.text = context.getString(com.doris.parkinginfo.R.string.payex, parkingInfo.payex)
        setMapWebView(webview, parkingInfo.address)
    }

    fun setMapWebView(webview: WebView, address: String) {
        webview.settings.javaScriptEnabled = true
        webview.loadUrl("https://www.google.com/maps/place/" + address)
    }


}