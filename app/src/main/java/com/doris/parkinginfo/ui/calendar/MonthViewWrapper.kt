package com.doris.parkinginfo.ui.calendar

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewTreeObserver
import android.widget.CheckBox
import android.widget.FrameLayout
import com.doris.parkinginfo.R
import java.time.LocalDate
import java.time.YearMonth

class MonthViewWrapper(context: Context,val attrs: AttributeSet, defStyle: Int) : FrameLayout(context, attrs, defStyle) {
    private var dayWidth = 0f
    private var dayHeight = 0f
    private var weekDaysLetterHeight = 0
    private var horizontalOffset = 0
    lateinit var monthView: MonthView
    private var days = ArrayList<DayMonthly?>()
    private var dayClickCallback: ((day: DayMonthly) -> Unit)? = null
    private var DAYS_CNT = 42
    private var startWeek = 0
    private var inflater: LayoutInflater
    private var year = 1911
    private var month = 1
    private var day = 1
    lateinit var yearMonthObject: YearMonth

    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0)

    init {
        val normalTextSize = resources.getDimensionPixelSize(R.dimen.normal_text_size).toFloat()
        weekDaysLetterHeight = 2 * normalTextSize.toInt()
        inflater = LayoutInflater.from(context)

        onGlobalLayout {
            measureSizes()
            //days = getDays()
            addViews()
           // monthView.updateDays(days)
           // addView(monthView)
        }
    }

    fun reset(y: YearMonth) {
        Log.d("Doris", y.year.toString() + " " + y.monthValue)
        DAYS_CNT = daysOfMonth(y.year, y.monthValue)
        startWeek = dayOfWeek(y.year, y.monthValue, 1)
        DAYS_CNT = DAYS_CNT + startWeek
        measureSizes()
        days = getDays()
        addViews()
        Log.e("Doris days", days.size.toString())
        monthView = MonthView(context, attrs)
        monthView.updateDays(days)

    }

    fun nextMothly() {
        yearMonthObject = yearMonthObject.plusMonths(1)
        reset(yearMonthObject)

    }

    fun preMothly() {
        yearMonthObject = yearMonthObject.minusMonths(1)
        reset(yearMonthObject)
    }

    fun startDate(_year: Int, _month: Int, _day: Int) {
        year = _year
        month = _month
        day = _day
        yearMonthObject = YearMonth.of(year, month)
        reset(yearMonthObject)
    }

    private fun getDays(): ArrayList<DayMonthly?> {
        val days = ArrayList<DayMonthly?>(DAYS_CNT)
        for (i in 0 until DAYS_CNT) {
            var day: DayMonthly? = DayMonthly(i + 1)
            if (i < startWeek) {
                day = null
            } else {
                day = DayMonthly(i + 1 - startWeek)
            }
            days.add(day)
        }
        return days
    }

    fun daysOfMonth(year: Int, month: Int): Int {
        val yearMonthObject = YearMonth.of(year, month)
        return yearMonthObject.lengthOfMonth()
    }

    fun dayOfWeek(year: Int, month: Int, day: Int): Int {
        val localDate = LocalDate.of(2019, 5, 1)
        return localDate.dayOfWeek.value
    }

    private fun measureSizes() {
        dayWidth = (width - horizontalOffset) / 7f
        dayHeight = dayWidth
    }

    private fun addViews() {
        removeAllViews()
        var curId = 0
        for (y in 0..5) {
            for (x in 0..6) {
                val day = days.getOrNull(curId)
                if (day != null) {
                    val xPos = x * dayWidth + horizontalOffset
                    val yPos = y * dayHeight + weekDaysLetterHeight
                    addViewBackground(xPos, yPos, day)
                }
                curId++
            }
        }
    }

    var foucsView: MonthBorder? = null

    private fun addViewBackground(xPos: Float, yPos: Float, day: DayMonthly) {
        inflater.inflate(R.layout.month_view_background, this, false).apply {
            layoutParams.width = dayWidth.toInt()
            layoutParams.height = dayHeight.toInt()
            x = xPos
            y = yPos

            setOnClickListener {
                foucsView?.apply {
                    cancelFocus()
                }
                dayClickCallback?.invoke(day)
                foucsView = it.findViewById(R.id.month_view_background) as MonthBorder
                (findViewById(R.id.checkbox) as CheckBox).isChecked = !(findViewById(R.id.checkbox) as CheckBox).isChecked
                foucsView?.setFocus()
                //setBackgroundColor(Color.GREEN)
            }
            addView(this)
        }

    }

    fun View.onGlobalLayout(callback: () -> Unit) {
        viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                callback()
            }
        })
    }

}