package com.doris.parkinginfo.ui.calendar

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.test_day.*
import kotlinx.android.synthetic.main.top_navigation.*

import java.time.YearMonth
import java.time.format.DateTimeFormatter


class TestActivity : AppCompatActivity(){
    val date = "2019/05"
    val dateFormat = DateTimeFormatter.ofPattern("yyyy/MM")
    var yearMonth  = YearMonth.parse(date, dateFormat)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.doris.parkinginfo.R.layout.test_day)
        month.startDate(2019,5,6)
        initView()
    }

    private fun initView(){
        top_value.text = yearMonth.toString()
        top_left_arrow.setOnClickListener {
            yearMonth = yearMonth.minusMonths(1)
            top_value.text = yearMonth.toString()
            month.preMothly()
        }

        top_right_arrow.setOnClickListener {
            yearMonth = yearMonth.plusMonths(1)
            top_value.text = yearMonth.toString()
            month.nextMothly()

        }
    }



}