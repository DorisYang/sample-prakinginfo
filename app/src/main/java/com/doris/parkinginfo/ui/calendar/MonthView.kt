package com.doris.parkinginfo.ui.calendar

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.CalendarView
import android.widget.TextView
import java.time.YearMonth
import kotlin.collections.ArrayList
import java.time.LocalDate


class MonthView(context: Context, attrs: AttributeSet, defStyle: Int) : View(context, attrs, defStyle) {
    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0)

    private val ROW_COUNT = 6
    private var dayWidth = 0f
    private var dayHeight = 0f
    private var textColor = Color.WHITE
    private var weekDaysLetterHeight = 0
    private var dayLetters = ArrayList<String>()
    private var horizontalOffset = 0
    private var paint: Paint
    private var gridPaint: Paint
    private var days = ArrayList<DayMonthly?>()
    private var DAYS_CNT = 42
    private var startWeek = 0
    private var dayClickCallback: ((day: DayMonthly) -> Unit)? = null


    init {
        val normalTextSize = resources.getDimensionPixelSize(com.doris.parkinginfo.R.dimen.normal_text_size)
        weekDaysLetterHeight = normalTextSize * 2

        paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = Color.rgb(255, 115, 0)
            textSize = normalTextSize.toFloat()
            textAlign = Paint.Align.CENTER
        }
        gridPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = Color.GRAY
        }

        initWeekDayLetters()
    }

    fun updateDays(newDays: ArrayList<DayMonthly?>) {
        days = newDays
        invalidate()
    }

    private fun initWeekDayLetters() {
        dayLetters = context.resources.getStringArray(com.doris.parkinginfo.R.array.week_day_letters).toMutableList() as ArrayList<String>
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        Log.d("Doris", "onDraw")
        measureDaySize(canvas)
        addWeekDayLetters(canvas)
        var row = 6
        var curId = 0
        loop@ for (y in 0 until ROW_COUNT) {
            for (x in 0..6) {
                val day = days.getOrNull(curId)
                if (day != null) {
                    val verticalOffset = weekDaysLetterHeight + dayHeight / 2
                    val xPos = x * dayWidth + horizontalOffset
                    val yPos = y * dayHeight + verticalOffset
                    val xPosCenter = xPos + dayWidth / 2
                    canvas.drawText(day.value.toString(), xPosCenter, yPos + paint.textSize, getTextPaint(day))
                } else if (y > 0 && x == 0) {
                    row = y
                    break@loop
                }
                curId++
            }
        }
        drawGrid(canvas, row)

    }

    private fun getTextPaint(startDay: DayMonthly): Paint {
        var paintColor = textColor
        return getColoredPaint(paintColor)
    }

    private fun getColoredPaint(color: Int): Paint {
        val curPaint = Paint(paint)
        curPaint.color = color
        return curPaint
    }

    private fun addWeekDayLetters(canvas: Canvas) {
        for (i in 0..6) {
            val xPos = horizontalOffset + (i + 1) * dayWidth - dayWidth / 2
            var weekDayLetterPaint = paint
            canvas.drawText(dayLetters[i], xPos, weekDaysLetterHeight * 0.7f, weekDayLetterPaint)
        }
    }

    private fun measureDaySize(canvas: Canvas) {
        dayWidth = (canvas.width - horizontalOffset) / 7f
        dayHeight = dayWidth
    }

    private fun drawGrid(canvas: Canvas, row_count: Int) {
        // vertical lines
        for (i in 0..6) {
            var lineX = i * dayWidth
            canvas.drawLine(lineX, 0f, lineX, row_count * dayHeight + weekDaysLetterHeight
                    , gridPaint)
        }

        // horizontal lines
        canvas.drawLine(0f, 0f, canvas.width.toFloat(), 0f, gridPaint)
        for (i in 0..5) {
            canvas.drawLine(0f, i * dayHeight + weekDaysLetterHeight, canvas.width.toFloat(), i * dayHeight + weekDaysLetterHeight, gridPaint)
        }
    }

}
