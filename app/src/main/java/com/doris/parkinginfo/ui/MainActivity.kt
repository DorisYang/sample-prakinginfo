package com.doris.parkinginfo.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.widget.Toast
import com.doris.parkinginfo.R
import com.doris.parkinginfo.ui.adapter.ParkingInfoExpandableListAdapter
import com.doris.parkinginfo.ui.common.CustomDialog.closeLoadingDialog
import com.doris.parkinginfo.ui.common.CustomDialog.showLoadingDialog
import com.doris.parkinginfo.ui.common.CustomDialog.showParkingInfoDetailDialog
import com.doris.parkinginfo.util.Status

import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()


    }


    fun initView() {
        var adapter: ParkingInfoExpandableListAdapter? = null
        mainViewModel.getParkingInfo().observe(this, Observer {
            it?.let { body ->
                when (body.status) {
                    Status.SUCCESS -> {
                        closeLoadingDialog()
                        if (body.data.isNullOrEmpty()) {
                            Toast.makeText(this, "isNullOrEmpty", Toast.LENGTH_SHORT).show()
                        } else {
                            adapter = ParkingInfoExpandableListAdapter(body.data)
                            expandable_list_view.setAdapter(adapter)
                        }
                    }

                    Status.ERROR -> {
                        closeLoadingDialog()
                        Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
                    }

                    Status.LOADING -> {
                        showLoadingDialog(this)
                    }
                }
            }
        })

        expandable_list_view.setOnChildClickListener { listView, view, groupPosition, childPosition, l ->
            if (adapter != null && adapter is ParkingInfoExpandableListAdapter) {
                val parkingInfo = adapter!!.getChild(groupPosition, childPosition)
                showParkingInfoDetailDialog(this, parkingInfo)
            }
            return@setOnChildClickListener false
        }
    }


}
