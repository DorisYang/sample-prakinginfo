package com.doris.parkinginfo.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}