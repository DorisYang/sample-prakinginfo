package com.doris.parkinginfo.vo

import com.google.gson.annotations.SerializedName

data class ParkingInfo(
    @SerializedName("ID")
    val id: String,
    @SerializedName("NAME")
    val name: String,
    @SerializedName("AREA")
    val area: String,
    @SerializedName("TYPE")
    val type: String,
    @SerializedName("SUMMARY")
    val summary: String,
    @SerializedName("ADDRESS")
    val address: String,
    @SerializedName("TEL")
    val tel: String,
    @SerializedName("PAYEX")
    val payex: String,
    @SerializedName("SERVICETIME")
    val serviceTime: String,
    @SerializedName("TW97X")
    val tw97x: String,
    @SerializedName("TW97Y")
    val tw97y: String,
    @SerializedName("TOTALCAR")
    val totalcar: String,
    @SerializedName("TOTALMOTOR")
    val totalMotor: String,
    @SerializedName("TOTALBIKE")
    val totalBike: String
)